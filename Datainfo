The Data:
This data comes from a water quality study [1] where samples were taken from sites on
different European rivers during a period of approximately one year. These samples were
analysed for various chemical substances and, in parallel, algae samples were collected to
determine the algae population distributions for seven algal species.
The impact on the environment of toxic waste, from a wide variety of manufacturing processes,
is well known. It has also become clear that the subtler effects of nutrient level and chemical
balance changes arising from farming land run-off and sewage water treatment also have a
serious, but indirect, effect on the states of rivers, lakes and even the sea. The influence of
season, river size and river flow rates are also considered important.
There are a total of 200 cases (rows/rivers) each containing 14 values (columns/variables).
The first 3 variables in the data set are the season, river size, and fluid velocity of the river.
The next six variables are the chemical concentrations: nitrogen, nitrates, nitrites, ammonia,
phosphate and oxygen (all measured in mg/L). The last five values of each row are the
amount of different kinds of algae. These 5 kinds are only a very small part of the whole algae
community. The value 0.0 means that the frequency is very low.
The data set also contains some missing data which are labelled with the string XXXXX.
A consultancy firm has asked you to explore this data and address three specific aspects of
interest (Tasks 1, 2 and 3 below) for their client, and then report your process (what you
have done and why) and findings in a written report. Before beginning the Tasks, you may
need to do some data cleaning due to missing data. You must only use statistical methods
covered in this course to address the tasks.
Reference: [1] Lichman, M. (2013). UCI Machine Learning Repository [http://archive.ics.uci.
edu/ml]. Irvine, CA: University of California, School of Information and Computer Science
